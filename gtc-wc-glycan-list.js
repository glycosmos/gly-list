import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import 'gtc-wc-summary/gtc-wc-summary.js';
import 'gtc-wc-image/gtc-wc-image.js';
import 'gtc-wc-glycoct/gtc-wc-glycoct.js';
import 'gtc-wc-wurcs/gtc-wc-wurcs.js';

class GtcWcGlycanList extends PolymerElement {
  static get template() {
    return html`
<style>
/* shadow DOM styles go here */:

</style>

<iron-ajax auto url="https://test.sparqlist.glycosmos.org/sparqlist/api/gtc_accession_list?offset={{offset}}&limit={{limit}}" handle-as="json" last-response="{{sampleids}}"></iron-ajax>
  <div>
    <table border="1">
      <thead>
        <tr>
          <th>Summary</th>
          <th>Image</th>
          <th>GlycoCT</th>
          <th>WURCS</th>
        </tr>
      </thead>
      <tbody>
        <template is="dom-repeat" items="{{sampleids}}">
          <tr>
            <td><gtc-wc-summary accession="{{item.AccessionNumber}}"></gtc-wc-summary></td>
            <td><gtc-wc-image accession="{{item.AccessionNumber}}"></gtc-wc-image></td>
            <td><gtc-wc-glycoct accession="{{item.AccessionNumber}}"></gtc-wc-glycoct></td>
            <td><gtc-wc-wurcs accession="{{item.AccessionNumber}}"></gtc-wc-wurcs></td>
            <!-- <td><gfc-error-message hash_key="{{item.hash}}"></gfc-error-message></td> -->
          </tr>
        </template>
      </tbody>
    <table>
  <div>
   `;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      offset: {
        notify: true,
        type: String
      },
      limit: {
        notify: true,
        type: String
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('gtc-wc-glycan-list', GtcWcGlycanList);
