# Developmet Document

[Google Doc](https://docs.google.com/document/d/1z9SS6rLaFJeUc9Uf81UMOrLrXZ1RYOnmsyEzoPXPyIQ/edit?usp=sharing)
上記のGoogle Docによる情報共有を廃止し、以下の仕様書をまとめる

[仕様書](https://gitlab.com/glycosmos/gly-develop/blob/master/List%20&%20Entry/entry.md)

上記の仕様書と並行して設計書を並行して更新していく、設計書が煮詰まり次第、開発を行う。
全体の効率を考え、仕様設計を策定するために必要な調査を行う必要があるため、開発に必要なサンプルコードは随時書いていく。

[設計書]((https://gitlab.com/glycosmos/gly-develop/blob/master/List%20&%20Entry/entry.md))